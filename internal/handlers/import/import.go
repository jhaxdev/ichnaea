package importHandler

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/jhax/ichnaea/database"
	"gitlab.com/jhax/ichnaea/internal/model"
)

func ImportCSV(c *fiber.Ctx) error {
	db := database.DB
	var prices []model.Price

	// find all prices in the database
	db.Find(&prices)

	// If no price is present return an error
	if len(prices) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No prices present", "data": nil})
	}

	// Else return prices
	return c.JSON(fiber.Map{"status": "success", "message": "Prices Found", "data": prices})
}
