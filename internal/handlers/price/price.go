package priceHandler

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/jhax/ichnaea/database"
	"gitlab.com/jhax/ichnaea/internal/model"
)

func GetPrices(c *fiber.Ctx) error {
	db := database.DB
	var prices []model.Price

	// find all prices in the database
	db.Find(&prices)

	// If no price is present return an error
	if len(prices) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No prices present", "data": nil})
	}

	// Else return prices
	return c.JSON(fiber.Map{"status": "success", "message": "Prices Found", "data": prices})
}

func CreatePrices(c *fiber.Ctx) error {
	db := database.DB
	price := new(model.Price)

	// Store the body in the price and return error if encountered
	err := c.BodyParser(price)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	// Add a uuid to the price
	price.ID = uuid.New()
	// Create the Price and return error if encountered
	err = db.Create(&price).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create price", "data": err})
	}

	// Return the created price
	return c.JSON(fiber.Map{"status": "success", "message": "Created Price", "data": price})
}

func GetPrice(c *fiber.Ctx) error {
	db := database.DB
	var price model.Price

	// Read the param priceId
	id := c.Params("priceId")

	// Find the price with the given Id
	db.Find(&price, "id = ?", id)

	// If no such price present return an error
	if price.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No price present", "data": nil})
	}

	// Return the price with the Id
	return c.JSON(fiber.Map{"status": "success", "message": "Prices Found", "data": price})
}

func UpdatePrice(c *fiber.Ctx) error {
	type updatePrice struct {
		Ticker string  `json:"Ticker"`
		Time   uint64  `json:"Time"`
		Open   float64 `json:"Open"`
		High   float64 `json:"High"`
		Low    float64 `json:"Low"`
		Close  float64 `json:"Close"`
		Volume float64 `json:"Volume"`
	}
	db := database.DB
	var price model.Price

	// Read the param priceId
	id := c.Params("priceId")

	// Find the price with the given Id
	db.Find(&price, "id = ?", id)

	// If no such price present return an error
	if price.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No price present", "data": nil})
	}

	// Store the body containing the updated data and return error if encountered
	var updatePriceData updatePrice
	err := c.BodyParser(&updatePriceData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	// Edit the price
	price.Ticker = updatePriceData.Ticker
	price.Time = updatePriceData.Time
	price.Open = updatePriceData.Open
	price.High = updatePriceData.High
	price.Low = updatePriceData.Low
	price.Close = updatePriceData.Close
	price.Volume = updatePriceData.Volume

	// Save the Changes
	db.Save(&price)

	// Return the updated price
	return c.JSON(fiber.Map{"status": "success", "message": "Prices Found", "data": price})
}

func DeletePrice(c *fiber.Ctx) error {
	db := database.DB
	var price model.Price

	// Read the param priceId
	id := c.Params("priceId")

	// Find the price with the given Id
	db.Find(&price, "id = ?", id)

	// If no such price present return an error
	if price.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No price present", "data": nil})
	}

	// Delete the price and return error if encountered
	err := db.Delete(&price, "id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete price", "data": nil})
	}

	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Price"})
}
