package productHandler

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/jhax/ichnaea/database"
	"gitlab.com/jhax/ichnaea/internal/model"
)

func GetProducts(c *fiber.Ctx) error {
	db := database.DB
	var products []model.Product

	// find all products in the database
	db.Find(&products)

	// If no product is present return an error
	if len(products) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No products present", "data": nil})
	}

	// Else return products
	return c.JSON(fiber.Map{"status": "success", "message": "Products Found", "data": products})
}

func CreateProducts(c *fiber.Ctx) error {
	db := database.DB
	product := new(model.Product)

	// Store the body in the product and return error if encountered
	err := c.BodyParser(product)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	// Add a uuid to the product
	product.ID = uuid.New()
	// Create the Product and return error if encountered
	err = db.Create(&product).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create product", "data": err})
	}

	// Return the created product
	return c.JSON(fiber.Map{"status": "success", "message": "Created Product", "data": product})
}

func GetProduct(c *fiber.Ctx) error {
	db := database.DB
	var product model.Product

	// Read the param productId
	id := c.Params("productId")

	// Find the product with the given Id
	db.Find(&product, "id = ?", id)

	// If no such product present return an error
	if product.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No product present", "data": nil})
	}

	// Return the product with the Id
	return c.JSON(fiber.Map{"status": "success", "message": "Products Found", "data": product})
}

func UpdateProduct(c *fiber.Ctx) error {
	type updateProduct struct {
		Name          string
		Store         string
		Sku           string
		RetailPrice   float32
		PurchasePrice float32
		IsTracked     bool
	}
	db := database.DB
	var product model.Product

	// Read the param productId
	id := c.Params("productId")

	// Find the product with the given Id
	db.Find(&product, "id = ?", id)

	// If no such product present return an error
	if product.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No product present", "data": nil})
	}

	// Store the body containing the updated data and return error if encountered
	var updateProductData updateProduct
	err := c.BodyParser(&updateProductData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	// Edit the product
	product.Name = updateProductData.Name
	product.Store = updateProductData.Store
	product.Sku = updateProductData.Sku
	product.RetailPrice = updateProductData.RetailPrice
	product.PurchasePrice = updateProductData.PurchasePrice

	// Save the Changes
	db.Save(&product)

	// Return the updated product
	return c.JSON(fiber.Map{"status": "success", "message": "Products Found", "data": product})
}

func DeleteProduct(c *fiber.Ctx) error {
	db := database.DB
	var product model.Product

	// Read the param productId
	id := c.Params("productId")

	// Find the product with the given Id
	db.Find(&product, "id = ?", id)

	// If no such product present return an error
	if product.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No product present", "data": nil})
	}

	// Delete the product and return error if encountered
	err := db.Delete(&product, "id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete product", "data": nil})
	}

	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Product"})
}
