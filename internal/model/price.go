package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Price struct {
	gorm.Model           // Adds some metadata fields to the table
	ID         uuid.UUID `gorm:"type:uuid"` // Explicitly specify the type to be uuid
	Ticker     string
	Time       uint64  `gorm:"unique;not null;"`
	Open       float64 `sql:"type:decimal(10,2);"`
	High       float64 `sql:"type:decimal(10,2);"`
	Low        float64 `sql:"type:decimal(10,2);"`
	Close      float64 `sql:"type:decimal(10,2);"`
	Volume     float64 `sql:"type:decimal(10,2);"`
}
