package model

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	ID            uuid.UUID `gorm:"type:uuid"` // Explicitly specify the type to be uuid
	Name          string
	Store         string
	Sku           string
	RetailPrice   float32 `sql:"type:decimal(10,2);"`
	PurchasePrice float32 `sql:"type:decimal(10,2);"`
	ProductImage  string
	IsTracked     bool      `sql:"type:boolean;"`
	CreatedAt     time.Time // Set to current time if it is zero on creating
	UpdatedAt     time.Time
}
