package priceRoutes

import (
	"github.com/gofiber/fiber/v2"
	priceHandler "gitlab.com/jhax/ichnaea/internal/handlers/price"
)

func SetupPriceRoutes(router fiber.Router) {
	price := router.Group("/prices")
	// Create a Price
	price.Post("/", priceHandler.CreatePrices)
	// Read all Prices
	price.Get("/", priceHandler.GetPrices)
	// // Read one Price
	price.Get("/:priceId", priceHandler.GetPrice)
	// // Update one Price
	price.Put("/:priceId", priceHandler.UpdatePrice)
	// // Delete one Price
	price.Delete("/:priceId", priceHandler.DeletePrice)
}
