package productRoutes

import (
	"github.com/gofiber/fiber/v2"
	productHandler "gitlab.com/jhax/ichnaea/internal/handlers/product"
)

func SetupProductRoutes(router fiber.Router) {
	product := router.Group("/products")
	// Create a Product
	product.Post("/", productHandler.CreateProducts)
	// Read all Products
	product.Get("/", productHandler.GetProducts)
	// // Read one Product
	product.Get("/:productId", productHandler.GetProduct)
	// // Update one Product
	product.Put("/:productId", productHandler.UpdateProduct)
	// // Delete one Product
	product.Delete("/:productId", productHandler.DeleteProduct)
}
