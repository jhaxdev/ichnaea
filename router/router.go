package router

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	priceRoutes "gitlab.com/jhax/ichnaea/internal/routes/price"
	productRoutes "gitlab.com/jhax/ichnaea/internal/routes/product"
)

func SetupRoutes(app *fiber.App) {
	api := app.Group("/api", logger.New())

	// Setup the Node Routes
	priceRoutes.SetupPriceRoutes(api)
	productRoutes.SetupProductRoutes(api)
}
